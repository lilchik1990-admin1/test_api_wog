﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API_AT.API.Models
{
    public class RegistrationModel
    {
        public Registration_Data Registration_Data { get; set; }
    }
    public class Registration_Data
    {
        public string message { get; set; }
        public string cause { get; set; }

        public string timestamp { get; set; }
        public int status { get; set; }
        public string error { get; set; }
        public string path { get; set; }


    }
}
