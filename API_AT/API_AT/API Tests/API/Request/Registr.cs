﻿using API_AT.API.Models;
using API_AT.Support;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace API_AT.API.Request
{
    internal static class Registr
    {
        internal static HttpStatusCode RegistrationPost(int age, string email, string login, string password)
        {
            ApiRequest.ConfigureApiRequest("http://localhost:4221/registration");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"age", age},
                {"email", email },
                {"login", login },
                {"password", password}
            };
            ApiRequest.SetData(parameters);

            var response = ApiRequest.SendRequest();
            return response.StatusCode;
        }
    }
}
