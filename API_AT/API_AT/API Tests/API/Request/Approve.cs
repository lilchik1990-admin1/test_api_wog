﻿using API_AT.Support;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace API_AT.API.Request
{
    public class Approve
    {
        internal static HttpStatusCode ApproveNewBalance(string token, bool approve, string userLogin)
        {
            ApiRequest.ConfigureApiRequest("http://localhost:4221/approve-money");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"approve", approve},
                {"userLogin", userLogin}
            };            
            var headers = new Dictionary<string, string>
            {
                {"Authorization", token},
                {"content-type", "application/json"}
            };
            ApiRequest.SetData(parameters, headers);

            var response = ApiRequest.SendRequest();
            return response.StatusCode;
        }
    }
}
