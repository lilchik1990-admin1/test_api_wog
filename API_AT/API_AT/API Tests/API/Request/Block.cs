﻿using API_AT.Support;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace API_AT.API.Request
{
    public class Block
    {
        internal static HttpStatusCode BlockPost(bool wasBlock, string login, string token)
        {
            ApiRequest.ConfigureApiRequest("http://localhost:4221/block-unblock-user");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"enable", wasBlock},
                {"userLogin", login}
            };
            var headers = new Dictionary<string, string>
            {
                {"Authorization", token},
                {"content-type", "application/json"}
            };
            ApiRequest.SetData(parameters, headers);

            var response = ApiRequest.SendRequest();
            return response.StatusCode;
        }
    }
}
