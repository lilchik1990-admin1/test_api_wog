﻿using API_AT.Support;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace API_AT.API.Request
{
    class BalanceReq
    {
        internal static IRestResponse GetBalance(string token)
        {
            ApiRequest.ConfigureApiRequest("http://localhost:4221/get-balance");
            ApiRequest.SetRequestType(Method.GET);
            var headers = new Dictionary<string, string>
            {
                {"Authorization", token},
                {"content-type", "application/json"}
            };
            ApiRequest.SetData(null, headers);

            var response = ApiRequest.SendRequest();
            return response;
        }
    }
}
