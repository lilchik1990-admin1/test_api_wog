﻿using API_AT.Support;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace API_AT.API.Request
{
    public class SendMoney
    {
        internal static HttpStatusCode TopUpWallet(int sum, string token)
        {
            ApiRequest.ConfigureApiRequest("http://localhost:4221/send-money-request");
            ApiRequest.SetRequestType(Method.POST);
            var parameters = new Dictionary<string, object>
            {
                {"requestMoney", sum}
            };
            var headers = new Dictionary<string, string>
            {
                {"Authorization", token},
                {"content-type", "application/json"}
            };
            ApiRequest.SetData(parameters, headers);

            var response = ApiRequest.SendRequest();
            return response.StatusCode;
        }
    }
}
