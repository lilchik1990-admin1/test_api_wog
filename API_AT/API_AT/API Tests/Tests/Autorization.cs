using API_AT.API.Request;
using NUnit.Framework;
using System.Net;

namespace API_AT
{
    public class Autorization
    {
        [TestCase("Cevoni03781", "n49bpxeCYGALb")]
        public void ValidAutorization(string login, string password)
        {
            var responseStatusCode = Auth.AuthPost(login, password);
            //var actualTocken = Auth.AuthToken(login, password);

            Assert.AreEqual(HttpStatusCode.OK, responseStatusCode);
        }

        [TestCase("Admin", "000GGGggg000")]
        public void AutorizationAdmin(string login, string password)
        {
            var responseStatusCode = Auth.AuthPost(login, password);
            //var actualTocken = Auth.AuthToken(login, password);

            Assert.AreEqual(HttpStatusCode.OK, responseStatusCode);
        }
        [TestCase("Cevoni", "n49bpxeCYGALb")]
        public void InvalidAutorization(string login, string password)
        {
            var actualStatusCode = Auth.AuthPost(login, password);

            Assert.AreEqual(HttpStatusCode.Unauthorized, actualStatusCode);
        }

        
    }
}