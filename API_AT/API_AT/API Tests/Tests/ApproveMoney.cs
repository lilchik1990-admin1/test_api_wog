﻿using API_AT.API.Request;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace API_AT
{
    /// <summary>
    /// The controller is responsible for approve/unapprove user request to client admin for money.
    /// </summary>
    class ApproveMoney
    {
        [TestCase("KlientAdmin1", "HelloSpring1", true, "Cevoni0378")]//klient admin 1
        [TestCase("KlientAdmin2", "HelloSpring1", true, "nAstya531013185")]//klient admin 2
        public void ApproveClientAdmin(string login, string password, bool approve, string userLogin)
        {
            var token = Auth.AuthToken(login, password); 
            var responseStatusCode = Approve.ApproveNewBalance(token, approve, userLogin);
            //
            Assert.AreEqual(HttpStatusCode.OK, responseStatusCode);
        }
    }
}
