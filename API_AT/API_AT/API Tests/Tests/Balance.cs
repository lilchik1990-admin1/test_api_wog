﻿using API_AT.API.Request;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace API_AT
{
    /// <summary>
    /// The controller is responsible for get user balance.
    /// </summary>
    class Balance
    {
        [TestCase("Cevoni03781", "n49bpxeCYGALb", "10000.0")]
        [TestCase("lifE319068997", "n4938279iu2qLb", "10040.0")]
        public void GetBalanceUser(string login, string password, string balanse)
        {
            var actualTocken = Auth.AuthToken(login, password);
            var response = BalanceReq.GetBalance(actualTocken);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(balanse, response.Content);
        }

        //[TestCase("Cevoni03781", "n49bpxeCYGALb")]
        public void GetBalanceHistory(string login, string password)
        {
            var actualTocken = Auth.AuthToken(login, password);
            var responseStatusCode = BalanceReq.GetBalance(actualTocken);

            //Assert.AreEqual(HttpStatusCode.OK, responseStatusCode);//token
        }
    }
}
