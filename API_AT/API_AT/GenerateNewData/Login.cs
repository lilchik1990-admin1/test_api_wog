﻿using System;

namespace API_AT
{
    public static class Login
    {
        const string name0 = "test";
        const string name1 = "nAstya";
        const string name2 = "lifE";
        const string name3 = "TEST";
        const string name4 = "TEST1";
        const string name5 = "Tysya";

        public static string MakeLogin()
        {
            string randomLogin = "";
            Random random = new Random();
            var name = random.Next(0, 6);
            switch (name)
            {
                case 0:
                    randomLogin += name0;
                    break;
                case 1:
                    randomLogin += name1;
                    break;
                case 2:
                    randomLogin += name2;
                    break;
                case 3:
                    randomLogin += name3;
                    break;
                case 4:
                    randomLogin += name4;
                    break;
                case 5:
                    randomLogin += name5;
                    break;
            }
            var num = (DateTime.Now.ToBinary() * -1).ToString();
            num = num.Substring(9, 9);

            return randomLogin + num;
        }
    }
}
